package com.mobileApp.sample.testcase;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.Test;
import com.mobileApp.sample.utility.BaseClassMobile;
import com.mobileApp.sample.utility.CommanApp;

import io.appium.java_client.MobileElement;



//Inherit the property from parent class with extends keyword

public class mobileAppLogin extends BaseClassMobile {

	
	@Test(priority = 1)
	public void login() throws MalformedURLException, InterruptedException {

		// TODO Auto-generated method stub

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(3000);
		String AppSwitcher = driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='App Switcher']").getText();
		if(AppSwitcher.contains("App Switcher")) {
			Thread.sleep(1000);
			
		MobileElement  selectfromdropdown =driver.findElementByXPath("//*[@class='android.widget.Spinner' and @index='3']");
		selectfromdropdown.click();
		driver.findElementByXPath("//*[@class='android.widget.CheckedTextView' and @index='0']").click();
		Thread.sleep(1000);
		driver.findElementByXPath("//*[@class='android.widget.Spinner' and @index='5']").click();
		driver.findElementByXPath("//*[@class='android.widget.CheckedTextView' and @index='0']").click();
		
		Thread.sleep(1000);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Start Over']").click();
		}

		// parent class methods "caps" use in child class "twcLogin"
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		
		// Click on "Skip"

		driver.findElementByXPath("//android.widget.TextView[contains(@text,'SKIP')]").click();

		// Successfully login positive testcase
		CommanApp.MobileLogin(driver);

		System.out.println("Please Enter OTP");
         
		Thread.sleep(3000);

		driver.findElementByXPath("//*[@class='android.widget.TextView' and @index='2']").click();
		Thread.sleep(3000);

		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Login Without OTP']").click();

		System.out.println("OTP Verify Successfully");
	}
	
	
	@Test(priority = 2)
	public void App_onBoardingFollow() throws InterruptedException {
		// Verify Screen
		Thread.sleep(3000);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @index='1']").getText();
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @index='3']").getText();

		driver.findElementByXPath("//*[@class='android.view.ViewGroup' and @index='6']").click();

		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").click();
		Thread.sleep(3000);
		System.out.println("Congrats you're Verified");
		Thread.sleep(3000);

		// Click on "I Accept" button
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='I Accept']").click();

		System.out.println("Accept the Terms so Clear360 Can Start Protecting You");

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Enable Notifications']").click();
		System.out.println("Enable Notifications Successfully");

		Thread.sleep(3000);
		driver.findElementByXPath("//*[@class='android.widget.TextView' and @index='3']").getText();

		driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").click();
		
		
	}
		  
	@Test(priority = 3)
	public void CheckIn_With_No() throws InterruptedException {
		 
		
		
		Thread.sleep(1000);
		  CommanApp.scrollToAnElementBytExt(driver, "Submit");
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Submit']").click();
		  
		  Thread.sleep(1000);
		  
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Go to Dashboard']").click();
		  
		  Thread.sleep(1000);
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Check-In']").click();
		
	}
	
		
	@Test(priority = 4)
	public void CheckIn_Symptoms() throws InterruptedException {
		
		  // Check-in
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath( "//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='4']").click();
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='2']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='5']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(1000);
		  
		  // Get text 
		  String Cough = driver.findElementByXPath("//android.view.ViewGroup[@index='1']/android.widget.TextView[@text='Cough or worsening of chronic cough']").getText();
		  System.out.println(Cough);
		  
		  
		  Thread.sleep(1000); 
		  String fevera=driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@text='Fever']").getText(); 
		  System.out.println(fevera);
		  
		  
		  
		  Thread.sleep(3000); 
		  String feverStatus=driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@index='5']").getText(); 
		  System.out.println(feverStatus);
		  
		  Thread.sleep(1000); 
		  String Breath = driver.findElementByXPath("//android.view.ViewGroup[@index='2']/android.widget.TextView[@text='Shortness of Breath']").getText(); 
		  System.out.println(Breath);
		  
		  Thread.sleep(1000);
		  String Headache = driver.findElementByXPath("//android.view.ViewGroup[@index='3']/android.widget.TextView[@text='Headache']").getText();
		  System.out.println(Headache);
		  
		  Thread.sleep(1000); 
		  String Fatigue = driver.findElementByXPath("//android.view.ViewGroup[@index='4']/android.widget.TextView[@text='Fatigue']").getText(); 
		  System.out.println(Fatigue);
		

		Thread.sleep(1000);
		CommanApp.scrollToAnElementBytExt(driver, "Nausea and vomiting");
		
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Save']").click();
	}
	
	@Test(priority = 5)
	public void CheckIn_TestedPositive() throws InterruptedException {
		
		  
		  //Tested positive 
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='1']/android.view.ViewGroup[@index='4']").click();
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='3']").click();
		  
		  Thread.sleep(3000);
		  driver.findElementByXPath("//android.view.View[@index='2']").click();
		  
		  
		  Thread.sleep(3000);
		  driver.findElementByXPath("//android.widget.Button[@index='1']").click();
		  
		  Thread.sleep(1000); 
		  
		  String Positivetext =driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@text='I tested positive for COVID-19']").getText(); 
		  System.out.println(Positivetext);
		  
		  
		  Thread.sleep(1000); 
		  String PositiveDate =driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@index='0']").getText(); 
		  System.out.println(PositiveDate);
		  
		  
		  Thread.sleep(1000);
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Save']").click();
	}
		  
	
	@Test(priority = 6)
	public void CheckIn_Travel() throws InterruptedException {
		  //Travel 
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='2']/android.view.ViewGroup[@index='4']").click();
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='3']").click();
		  
		//date
		  Thread.sleep(1000);
		  driver.findElementByXPath("//android.view.View[@index='3']").click();
		  
		  
		  Thread.sleep(3000);
		  driver.findElementByXPath("//android.widget.Button[@index='1']").click();
		  
		  Thread.sleep(1000); 
		  String Taveltext =driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@text='I have traveled to another country since my last check-in']").getText(); 
		  System.out.println(Taveltext);
		  
		  
		  
		  Thread.sleep(1000); 
		  String TavelDate =driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@index='0']").getText(); 
		  System.out.println(TavelDate);
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Save']").click();
	}
	
	@Test(priority = 7)
	public void CheckIn_Exposure() throws InterruptedException {
		  
		  //Exposure 
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='3']/android.view.ViewGroup[@index='4']").click();
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='2']").click();
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.view.ViewGroup[@index='3']").click();
		  
		  Thread.sleep(1000);//date
		  driver.findElementByXPath("//android.view.View[@index='4']").click();
		  
		  
		  Thread.sleep(1000);
		  driver.findElementByXPath("//android.widget.Button[@index='1']").click();
		  
		  Thread.sleep(1000); 
		  String Exposuretext =driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@text='I have provided care or been in close contact with a person who is feeling unwell since my last check-in']").getText(); 
		  System.out.println(Exposuretext);
		  
		  Thread.sleep(1000); 
		  String ExposureDate =driver.findElementByXPath("//android.view.ViewGroup[@index='0']/android.widget.TextView[@index='0']").getText(); 
		  System.out.println(ExposureDate);
		  
		  
		  Thread.sleep(1000); 
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Save']").click();
		  
		
	}
	
	@Test(priority = 8)
	public void Dashboard() throws InterruptedException {
		  
		  Thread.sleep(3000);
		  CommanApp.scrollToAnElementBytExt(driver, "Submit");
		  
		  Thread.sleep(3000); 
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Submit']").click();
		  
		  Thread.sleep(1000);
		  
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Go to Dashboard']").click();
		  
		  Thread.sleep(1000);
		  driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Profile']").click();
		  
		  Thread.sleep(3000);
		  String ClearId = driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='1']").getText(); 
		  System.out.println(ClearId); 
		  //return ClearId;
		  
		  
	}

		
	}

