package com.mobileApp.sample.utility;



import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.FindsByAndroidUIAutomator;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class CommanApp  {
	
	
		
	public static AppiumDriver<MobileElement> driver;
	public static AndroidDriver<MobileElement> androidDriver;
		
	// Successfully login positive testcase
	
	public static void MobileLogin(AppiumDriver <MobileElement> driver){
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='0']").sendKeys("10000105");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.findElementByXPath("//*[@class='android.widget.Spinner' and @index='0']").click();

			driver.findElementByXPath("//*[@class='android.widget.CheckedTextView' and @text='+91']").click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElementByXPath("//*[@class='android.widget.EditText' and @index='2']").sendKeys("9024429799");
			
			driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Enter Your Mobile Number']").click();
			driver.findElementByXPath("//*[@class='android.widget.TextView' and @text='Next']").click();

			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			System.out.println("Successfully login");
	}
	
	//Scroll App for static text		

	public static WebElement scrollToAnElementBytExt(AppiumDriver <MobileElement> driver,String text){
		return ((FindsByAndroidUIAutomator<MobileElement>) driver).findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0))");

		}	
			
		}

		
