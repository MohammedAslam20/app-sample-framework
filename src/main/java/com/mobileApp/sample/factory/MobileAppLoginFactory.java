package com.mobileApp.sample.factory;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.mobileApp.sample.utility.BaseClassMobile;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidBy;

public class MobileAppLoginFactory extends BaseClassMobile{



	//This is a constructor, as every page need a base driver to find web elements
	public MobileAppLoginFactory (AndroidBy driver) {
		super();
	}
	

	@FindBy(how = How.XPATH, using = "//*[@class='android.widget.EditText' and @index='0']")     
	public AndroidElement input_UserName ;


	public AndroidElement getInput_UserName() {
		return input_UserName;
	}


	public void setInput_UserName(AndroidElement input_UserName) {
		this.input_UserName = input_UserName;
	}
	  
	 

	
}
